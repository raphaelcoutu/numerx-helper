package org.ciussse.numerxhelper;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;

import org.controlsfx.control.Notifications;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javafx.geometry.Pos;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.util.Duration;

public class ScreenCapturer {
	final Clipboard clipboard = Clipboard.getSystemClipboard();
	Thread eraserThread;

	public void call() {
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (Exception e) {
			e.printStackTrace();
		}

		BufferedImage screenshot = robot.createScreenCapture(new Rectangle(3840, 3840));

		LuminanceSource source = new BufferedImageLuminanceSource(screenshot);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

		try {
			Result result = new MultiFormatReader().decode(bitmap);

			Notifications.create().title("Scan réussi").text(result.getText()).hideAfter(Duration.seconds(2))
					.position(Pos.BASELINE_RIGHT).showInformation();

			ClipboardContent content = new ClipboardContent();
			content.putString(result.getText());
			clipboard.setContent(content);

//        	// Si le thread existe et est vivant, on l'arrête pour éviter d'effacer notre nouvelle sélection
			if (eraserThread != null && eraserThread.isAlive()) {
				eraserThread.interrupt();
			}
			eraserThread = new Thread(new ClipboardEraserTask());
			eraserThread.start();

		} catch (NotFoundException e) {

			Notifications.create().title("Erreur").text("Veuillez refaire un scan. Code QR non trouvé.")
					.hideAfter(Duration.seconds(5)).position(Pos.BASELINE_RIGHT).showError();

		}
	}
}
