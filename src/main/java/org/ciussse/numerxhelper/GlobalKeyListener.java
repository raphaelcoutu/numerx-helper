package org.ciussse.numerxhelper;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import javafx.application.Platform;

public class GlobalKeyListener implements NativeKeyListener {
	
	App app;

	public GlobalKeyListener(App app) {
		this.app = app;
		
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.OFF);
		logger.setUseParentHandlers(false);

		try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException e) {
			e.printStackTrace();
		}
		GlobalScreen.addNativeKeyListener(this);
	}

	@Override
	public void nativeKeyTyped(NativeKeyEvent nativeEvent) {

	}

	@Override
	public void nativeKeyPressed(NativeKeyEvent nativeEvent) {
		Platform.runLater(() -> {
			if (nativeEvent.getKeyCode() == NativeKeyEvent.VC_ALT) {
				this.app.takeScreenshot();
			}			
		});
	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent nativeEvent) {

	}

}
