package org.ciussse.numerxhelper;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.input.Clipboard;

public class ClipboardEraserTask extends Task<Void> {

	final Clipboard clipboard = Clipboard.getSystemClipboard();

	@Override
	protected Void call() throws Exception {
		Thread.sleep(15000);
		Platform.runLater(() -> {
			try {
				clipboard.clear();
			} catch (Exception e) {
				System.out.println(e);
			}
		});
		return null;
	}

}
