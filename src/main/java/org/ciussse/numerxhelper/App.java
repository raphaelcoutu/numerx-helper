package org.ciussse.numerxhelper;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application {
	
	Stage primaryStage;
	ScreenCapturer capturer = new ScreenCapturer();

    @Override
    public void start(Stage stage) throws Exception {
    	this.primaryStage = stage;
    	
    	new GlobalKeyListener(this);
    	
    	Scene scene = new Scene(new VBox(), 1, 1);
    	scene.setFill(Color.TRANSPARENT);
    	primaryStage.setTitle("NUMERx Helper");
    	primaryStage.initStyle(StageStyle.TRANSPARENT);
    	primaryStage.getIcons().add(new Image(App.class.getResourceAsStream("icon.png")));
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(e -> {
        	e.consume();
        	primaryStage.setIconified(true);
        });
        
        primaryStage.show();
        
    }

	public static void main(String[] args) {
		launch();
    }
	
	public void takeScreenshot() {
		capturer.call();
	}
}